For the development Node 16 LTS is used.

To build:

    npm install
    npm run build release

To build & prepare release:

    npm install
    npm run dist

The resulting `background.js`, `options.js` & `common.js` will be compiled with [esbuild][1] from typescript sources, bundled, and minified (together with source maps) to the `ext/` folder, which will be used to package extension with `web-ext`.

[1]: https://esbuild.github.io/
