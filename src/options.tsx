import { render } from 'preact';
import { Options } from './options/Options';
import './options/options.css';

if (isAndroid()) {
  document.documentElement.classList.add('android');
} else {
  document.documentElement.classList.add('desktop');
}

document.addEventListener('readystatechange', () => {
  if (document.readyState === 'complete') {
    render(<Options uiLanguage={browser.i18n.getUILanguage()} />, document.body);
  }
});
