import { isWebURL } from './background/isWebPageURL';
import { toGoogleTranslate } from './background/toGoogleTranslate';

declare const DEBUG: boolean;

// open options pane on startup
if (DEBUG) {
  browser.runtime.openOptionsPage();
}

const uiLanguage = browser.i18n.getUILanguage();
let storagePreferredLanguage: null | string = null;
browser.storage.sync.get('preferredLanguage').then(({ preferredLanguage }) => {
  if (preferredLanguage) {
    storagePreferredLanguage = preferredLanguage;
  }

  browser.storage.onChanged.addListener((changes, areaName) => {
    if (areaName === 'sync' && 'preferredLanguage' in changes) {
      storagePreferredLanguage = changes.preferredLanguage.newValue;
    }
  });
});

// const tabUrl = new Map<number, URL | void>();

browser.tabs.onUpdated.addListener(async (tabId, changeInfo, tab) => {
  if (DEBUG) {
    console.log('Updated tab: ' + tabId);
    console.log('Changed attributes: ');
    console.log(changeInfo);
    console.log('New tab Info: ');
    console.log(tab);
  }

  // if (changeInfo.url) {
  //   console.log('translate: setting', tabId, changeInfo.url);
  //   tabUrl.set(tabId, string2URL(changeInfo.url));
  // }

  if (!(changeInfo.url && isWebURL(changeInfo.url))) {
    return;
  }

  const activeTabs = await browser.tabs.query({
    active: true,
    currentWindow: true,
  });

  if (activeTabs[0].id === tabId) {
    browser.pageAction.show(tabId);
  }
});

browser.pageAction.onClicked.addListener((tab, info) => {
  if (tab.url && isWebURL(tab.url)) {
    const googTranslateUrl = toGoogleTranslate(
      tab.url,
      getPreferredLanguage(googleLanguageList, uiLanguage, storagePreferredLanguage).code,
      getPreferredLanguage(googleLanguageList, uiLanguage, null).code
    );

    browser.tabs.update(tab.id, {
      url: googTranslateUrl,
    });

    if (DEBUG) {
      console.log(`forwarded ${tab.url} to ${googTranslateUrl}`);
    }
  }
});

/* browser.webNavigation.onBeforeNavigate.addListener(
  details => {
    console.log('translate: onBeforeNavigate');
    // top level navigation
    if (details.frameId === 0) {
      const url = tabUrl.get(details.tabId);
      console.warn('translate: from ', String(url));
      console.warn('translate: to ', details);
      if (url) {
        if (
          details.url === 'https://mail.yandex.ru/' &&
          String(url) === 'https://ya.ru/'
        ) {
          browser.tabs.update(details.tabId, { url: 'https://wikipedia.org/' });
        }
      }
    }
  },
  {
    url: [{ urlPrefix: 'https://translate.google.com/website?' }],
  }
); */
