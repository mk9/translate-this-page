export const transformHostname = (url: URL): string => url.hostname.replaceAll('-', '--').replaceAll('.', '-');

export const toGoogleTranslate = (url: WebURLString, translateTo: string, interfaceLanguage: string): WebURLString => {
  const parsed = new URL(url);
  const resultPrefix = 'https://' + transformHostname(parsed) + '.translate.goog' + (parsed.pathname || '/');

  const hasSearch = Boolean(parsed.search);

  const resultSuffix =
    (hasSearch ? parsed.search + '&' : '?') +
    (parsed.protocol === 'http:' ? '_x_tr_sch=http&' : '') +
    (parsed.port ? `_x_tr_port=${parsed.port}&` : '') +
    `_x_tr_sl=auto&_x_tr_tl=${translateTo}&_x_tr_hl=${interfaceLanguage}`;

  return (resultPrefix + resultSuffix) as WebURLString;
};
