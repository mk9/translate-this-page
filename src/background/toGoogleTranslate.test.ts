/// <reference types="jest" />

import { toGoogleTranslate } from './toGoogleTranslate';

type SourceURL = string;
type ExpectedURL = string;
type Assertions = [SourceURL, ExpectedURL][];

const cases: Assertions = [
  ['http://google.cn/', 'https://google-cn.translate.goog/?_x_tr_sch=http&_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en'],
  [
    'http://127.0.0.1:8000/',
    'https://127-0-0-1.translate.goog/?_x_tr_sch=http&_x_tr_port=8000&_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en',
  ],
  [
    'http://foo-bar.com/?_x_tr_sch=http&_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=de&as%5Bsd%5D=asd',
    'https://foo--bar-com.translate.goog/?_x_tr_sch=http&_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=de&as%5Bsd%5D=asd&_x_tr_sch=http&_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en',
  ],
  [
    'https://github.com/Lusito/webextension-polyfill-ts/issues?q=is%3Aissue+is%3Aclosed',
    'https://github-com.translate.goog/Lusito/webextension-polyfill-ts/issues?q=is%3Aissue+is%3Aclosed&_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en',
  ],
];

cases.forEach(([source, expected], caseIndex) => {
  test(`case #${caseIndex + 1}`, () => {
    expect(toGoogleTranslate(<WebURLString>source, 'en', 'en')).toBe(expected);
  });
});
