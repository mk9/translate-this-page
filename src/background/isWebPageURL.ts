enum WebURLStringBrand {
  _ = '',
}

declare global {
  /** Nominal string type  */
  type WebURLString = WebURLStringBrand & string;
}

const WEB_PAGE_URL_RE = /^https?/;

export const isWebURL = (url: string): url is WebURLString => {
  try {
    const parsed = new URL(url);

    if (/translate\.goog$/.test(parsed.hostname)) {
      return false;
    }

    return WEB_PAGE_URL_RE.test(parsed.protocol);
  } catch {
    return false;
  }
};
