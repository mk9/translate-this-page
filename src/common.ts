import { getPreferredLanguage } from './common/getPreferredLanguage';
import { googleLanguageList } from './common/googleLanguageList';
import { isAndroid } from './common/isAndroid';
import { StorageEventEmitter } from './common/StorageEventEmitter';
import { type EventEmitterListener } from './common/utils/EventEmitter';

window.googleLanguageList = googleLanguageList;
window.isAndroid = isAndroid;
window.StorageEventEmitter = StorageEventEmitter;
window.getPreferredLanguage = getPreferredLanguage;

export { type StorageEventEmitter, type EventEmitterListener };
