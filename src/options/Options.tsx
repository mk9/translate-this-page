import { FunctionalComponent } from 'preact';
import { useEffect, useRef, useState } from 'preact/hooks';
import { type EventEmitterListener, type StorageEventEmitter as StorageEE } from '../common';
import { Select, SelectOption } from './Select';

type AppInternalState = {
  initialized: boolean;
  preferredLanguageIndex: number;
};

type OptionsProps = {
  uiLanguage: string;
};

const useOptions = (
  uiLanguage: string
): {
  initialized: boolean;
  preferredLanguageOptions: SelectOption[];
  preferredLanguageIndex: number;
  updatePreferredLanguage: (updatedPreferredLanguage: string) => void;
} => {
  const eeRef = useRef<StorageEE<string> | null>(null);
  const [{ initialized, preferredLanguageIndex }, setInternalState] = useState<AppInternalState>({
    initialized: false,
    preferredLanguageIndex: -1,
  });
  const preferredLanguageOptions = googleLanguageList.map(({ code, name }) => ({ id: code, value: name }));

  useEffect(() => {
    eeRef.current = new StorageEventEmitter('preferredLanguage');
    const { current: preferredLanguageEE } = eeRef;

    const listener: EventEmitterListener<typeof preferredLanguageEE> = newValue => {
      const selectedLanguage = getPreferredLanguage(googleLanguageList, uiLanguage, newValue);

      setInternalState({
        initialized: true,
        preferredLanguageIndex: googleLanguageList.indexOf(selectedLanguage),
      });
    };

    preferredLanguageEE.on(listener);

    return () => {
      preferredLanguageEE.off(listener);
    };
  }, []);

  return {
    initialized,
    preferredLanguageOptions,
    preferredLanguageIndex,
    updatePreferredLanguage(value) {
      eeRef.current?.set(value);
    },
  };
};

export const Options: FunctionalComponent<OptionsProps> = ({ uiLanguage }) => {
  const { initialized, preferredLanguageOptions, preferredLanguageIndex, updatePreferredLanguage } =
    useOptions(uiLanguage);

  if (!initialized) {
    return null;
  }

  return (
    <form>
      <div className="row">
        <label htmlFor="preferredLanguage">Preferred translation language:</label>
        <Select
          id="preferredLanguage"
          options={preferredLanguageOptions}
          selectedIndex={preferredLanguageIndex}
          handleChange={updatePreferredLanguage}
        />
      </div>
    </form>
  );
};
