import { FunctionalComponent } from 'preact';

export type SelectOption = {
  id: string;
  value: string;
};

type SelectProps = {
  id: string;
  selectedIndex: number;
  options: SelectOption[];
  handleChange: (value: SelectOption['value']) => void;
};

export const Select: FunctionalComponent<SelectProps> = ({ id, selectedIndex, options, handleChange }) => {
  return (
    <div className="select-wrapper">
      <span className="select-wrapper-mirror">{options[selectedIndex]?.value}</span>
      <select
        id={id}
        onChange={event => {
          handleChange((event.target as HTMLSelectElement).value);
        }}
      >
        {options.map(({ id, value }, optionIndex) => (
          <option key={id} value={id} selected={optionIndex === selectedIndex}>
            {value}
          </option>
        ))}
      </select>
    </div>
  );
};
