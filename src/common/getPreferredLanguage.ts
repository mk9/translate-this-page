import { GoogleLanguage } from './types';

export const toUILanguageSlugs = (languageTag: string): [string] | [string, string] => {
  const dashIndex = languageTag.indexOf('-');
  if (dashIndex !== -1) {
    return [languageTag, languageTag.slice(0, dashIndex)];
  }

  return [languageTag];
};

export const getPreferredLanguage = (
  googleLanguages: GoogleLanguage[],
  uiLanguage: string,
  settingsLanguage: null | string,
  defaultLanguage = 'en'
): GoogleLanguage => {
  const settingsMatch = googleLanguages.find(lang => lang.code === settingsLanguage);
  if (settingsMatch) {
    return settingsMatch;
  }

  const match = googleLanguages.find(lang => toUILanguageSlugs(uiLanguage).some(slug => slug === lang.code));

  return match ?? googleLanguages.find(lang => lang.code === defaultLanguage)!;
};
