import { type Browser } from 'webextension-polyfill-ts';
import { getPreferredLanguage as _getPreferredLanguage } from './getPreferredLanguage';
import { isAndroid as _isAndroid } from './isAndroid';
import { type StorageEventEmitter as _StorageEventEmitter } from './StorageEventEmitter';

export type GoogleLanguage = {
  code: string;
  name: string;
};

declare global {
  const DEBUG: boolean;
  const browser: Browser;
  const googleLanguageList: Window['googleLanguageList'];
  const isAndroid: typeof _isAndroid;
  const StorageEventEmitter: typeof _StorageEventEmitter;
  const getPreferredLanguage: typeof _getPreferredLanguage;

  interface Window {
    googleLanguageList: GoogleLanguage[];
    isAndroid: typeof isAndroid;
    StorageEventEmitter: typeof StorageEventEmitter;
    getPreferredLanguage: typeof getPreferredLanguage;
  }
}
