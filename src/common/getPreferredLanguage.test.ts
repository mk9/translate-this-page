/// <reference types="jest" />

import { getPreferredLanguage, toUILanguageSlugs } from './getPreferredLanguage';
import { googleLanguageList } from './googleLanguageList';

test('toUILanguageSlugs', () => {
  expect(toUILanguageSlugs('en-US')).toEqual(['en-US', 'en']);
  expect(toUILanguageSlugs('fr')).toEqual(['fr']);
  expect(toUILanguageSlugs('x-pig-latin')).toEqual(['x-pig-latin', 'x']);
});

test('getPreferredLanguage', () => {
  expect(getPreferredLanguage(googleLanguageList, 'fr-CA', null).code).toEqual('fr');
  expect(getPreferredLanguage(googleLanguageList, 'x-foo', null, 'de').code).toEqual('de');
  expect(getPreferredLanguage(googleLanguageList, 'x-foo', 'ja', 'de').code).toEqual('ja');
});
