import { type Storage } from 'webextension-polyfill-ts';
import { EventEmitter } from './utils/EventEmitter';

export class StorageEventEmitter<T = any> extends EventEmitter<T | null> {
  #key: string;

  constructor(key: string) {
    super();

    this.#key = key;

    browser.storage.onChanged.addListener(this.#handleChange);
    browser.storage.sync
      .get(key)
      .then(storage => {
        this.emit(storage[key] ?? null);
      })
      .catch(() => {
        this.emit(null);
      });
  }

  set(value: T | null): void {
    browser.storage.sync.set({ [this.#key]: value });
  }

  #handleChange = (changes: { [s: string]: Storage.StorageChange }, areaName: string): void => {
    const key = this.#key;

    if (areaName === 'sync' && Object.hasOwnProperty.call(changes, key)) {
      this.emit(changes[key].newValue);
    }
  };
}
