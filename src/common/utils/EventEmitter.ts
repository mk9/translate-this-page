type Listener<T> = (event: T) => void;

export class EventEmitter<T = any> {
  #listeners: Listener<T>[] = [];

  on = (listener: Listener<T>): void => {
    this.#listeners.push(listener);
  };

  off = (listener: Listener<T>): void => {
    this.#listeners.filter(x => x !== listener);
  };

  protected emit = (event: T): void => {
    for (const listener of this.#listeners) {
      listener(event);
    }
  };
}

export type EventEmitterListener<EE extends EventEmitter> = Parameters<EE['on']>[0];
