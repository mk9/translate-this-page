import esbuild from 'esbuild';
import path from 'path';

const COMMON_ESBUILD_OPTIONS = (watch, isRelease) => ({
  bundle: true,
  minify: isRelease,
  sourcemap: true,
  metafile: true,
  define: {
    DEBUG: !isRelease,
  },
  watch,
  assetNames: '[name]',
  loader: {
    '.svg': 'file',
  },
});

const dirname = new URL('.', import.meta.url).pathname;
const getPath = s => path.join(dirname, s);
const watcher = {
  onRebuild(error, result) {
    if (error) {
      console.error(printResult(error));
    } else {
      console.log(printResult(result));
    }
  },
};

const compileBackground = (watch, release) =>
  esbuild.build({
    ...COMMON_ESBUILD_OPTIONS(watch, release),
    entryPoints: [getPath('src/background.ts')],
    outfile: getPath('ext/background.js'),
    format: 'esm',
  });

const compileCommon = (watch, release) =>
  esbuild.build({
    ...COMMON_ESBUILD_OPTIONS(watch, release),
    entryPoints: [getPath('src/common.ts')],
    outfile: getPath('ext/common.js'),
  });

const compileOptions = (watch, release) =>
  esbuild.build({
    ...COMMON_ESBUILD_OPTIONS(watch, release),
    entryPoints: [getPath('src/options.tsx')],
    inject: [getPath('src/options/preact-shim.ts')],
    outfile: getPath('ext/options.js'),
    jsxFactory: 'h',
    jsxFragment: 'Fragment',
    format: 'esm',
  });

const printResult = result =>
  esbuild.analyzeMetafileSync(result.metafile, {
    color: true,
  });

const isRelease = process.argv.some(arg => arg === 'release');
const isWatch = process.argv.some(arg => arg === 'watch');

if (isWatch) {
  try {
    console.log(printResult(await compileBackground(watcher, isRelease)));
    console.log(printResult(await compileOptions(watcher, isRelease)));
    console.log(printResult(await compileCommon(watcher, isRelease)));
  } catch {
    process.exit(1);
  }

  console.log('watching for changes...');
} else {
  const once = async f => {
    console.log(printResult(await f(false, isRelease)));
  };

  try {
    once(compileBackground);
    once(compileOptions);
    once(compileCommon);
  } catch (error) {
    console.log('caught error', error);
  }
}
